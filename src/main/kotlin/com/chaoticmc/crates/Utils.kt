package com.chaoticmc.crates

import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.configuration.Configuration
import org.bukkit.inventory.ItemStack


fun item(type: Material = Material.AIR, amount: Int = 1, durability: Number = 0, name: String = "", lo: MutableList<String> = mutableListOf()): ItemStack {
  val item = ItemStack(type, amount, durability.toShort())
  item.itemMeta = item.itemMeta.apply {
    displayName = name.c()
    lore = lo.c()
  }
  return item
}

fun itemBuilder(builder: DSLItemBuilder.() -> Unit): ItemStack {
  val itemBuilder = DSLItemBuilder()
  itemBuilder.builder()
  return itemBuilder.build()
}

data class DSLItemBuilder(var item: Material = Material.AIR,
                          var amount: Int = 1,
                          var durability: Int = 0,
                          var dispName: String = "",
                          var lore: MutableList<String> = mutableListOf()) {
  fun build() = item(item, amount, durability, dispName, lore)
}

fun Configuration.string(path: String) = getString(path).c()

fun String.c(prefix: Char = '&'): String = ChatColor.translateAlternateColorCodes(prefix, this)

fun Iterable<String>.c(prefix: Char = '&'): MutableList<String> {
  val temp = mutableListOf<String>()
  forEach { temp.add(it.c(prefix)) }
  return temp
}