package com.chaoticmc.crates

import org.apache.commons.io.FilenameUtils
import org.bukkit.configuration.file.YamlConfiguration
import java.io.File

object CratesConfig {
  
  private val configs = hashMapOf<String, Pair<File, YamlConfiguration>>()
  
  fun initiate(): HashMap<String, Pair<File, YamlConfiguration>> {
    val instance = Crates.instance
    
    val dataFolder = File(instance.dataFolder, "crates")
    if (!instance.dataFolder.exists()) instance.dataFolder.mkdir()
    if (!dataFolder.exists()) dataFolder.mkdir()
    val files = hashMapOf(
      "common" to File(dataFolder, "common.yml"),
      "uncommon" to File(dataFolder, "uncommon.yml"),
      "rare" to File(dataFolder, "rare.yml"),
      "mystical" to File(dataFolder, "mystical.yml"),
      "legendary" to File(dataFolder, "legendary.yml"))
    files.values.forEach { file ->
      val fileName = FilenameUtils.removeExtension(file.name)
      if (!file.exists()) {
        file.createNewFile()
        val config = YamlConfiguration.loadConfiguration(file)
        addDefaults(config)
        configs[fileName] = file to config
      } else
        configs[fileName] = file to YamlConfiguration.loadConfiguration(file)
      save(fileName)
    }
    return configs
  }
  
  private fun save(config: String): Boolean {
    val pair = configs[config] ?: return false
    pair.second.save(pair.first)
    return true
  }
  
  fun addDefaults(config: YamlConfiguration) {
    config.set("items.item1.displayItem", "APPLE")
    config.set("items.item1.displayName", "&e&lBold Name")
    config.set("items.item1.chance", 20)
    config.set("items.item1.amount", 64)
    config.set("items.item1.previewSlot", 0)
    config.set("items.item1.lore", mutableListOf("&9Issa Lore!"))
    config.set("items.item1.enchants.DAMAGE_ALL", 5)
    config.set("items.item1.enchants.DURABILITY", 1)
    
    config.set("items.item1.function.type", "command")
    config.set("items.item1.function.command", "give %user% apple 64")
    
    config.set("items.item2.displayItem", "DIAMOND_SWORD")
    config.set("items.item2.displayName", "&e&lBold Name")
    config.set("items.item2.chance", 25)
    config.set("items.item2.amount", 1)
    config.set("items.item2.previewSlot", 44)
    config.set("items.item2.lore", mutableListOf("&9Issa Lore!"))
    config.set("items.item2.enchants.KNOCKBACK", 1)
    config.set("items.item2.enchants.SILK_TOUCH", 5)
    
    config.set("items.item2.function.type", "item")
    config.set("items.item2.function.material", "DIAMOND_SWORD")
    config.set("items.item2.function.amount", 1)
    config.set("items.item2.function.name", "&e&lBold Name")
    config.set("items.item2.function.lore", mutableListOf("&9Issa Lore!"))
    config.set("items.item2.function.enchantment.KNOCKBACK", 1)
    config.set("items.item2.function.enchantment.SILK_TOUCH", 2)
  }
}
