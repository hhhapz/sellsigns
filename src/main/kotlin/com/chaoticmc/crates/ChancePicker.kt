package com.chaoticmc.crates

import org.bukkit.inventory.ItemStack

object ChancePicker {
  fun getRandomByChance(items: MutableMap<Int, Pair<ItemStack, Operation>>): Pair<ItemStack, Operation> {
    val list = mutableListOf<Pair<ItemStack, Operation>>()
    items.forEach {
      for (i in 0..it.key) list.add(it.value)
    }
    list.shuffle()
    return list.first()
  }
}