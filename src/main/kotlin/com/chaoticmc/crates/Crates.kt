package com.chaoticmc.crates

import com.chaoticmc.crates.listeners.ClickListener
import com.chaoticmc.crates.listeners.InteractListener
import com.chaoticmc.crates.listeners.InventoryCloseListener
import org.bukkit.Bukkit
import org.bukkit.Material
import org.bukkit.SkullType
import org.bukkit.configuration.file.FileConfiguration
import org.bukkit.configuration.file.YamlConfiguration
import org.bukkit.enchantments.Enchantment
import org.bukkit.entity.Player
import org.bukkit.inventory.Inventory
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.SkullMeta
import org.bukkit.plugin.java.JavaPlugin
import java.io.File
import java.util.concurrent.atomic.AtomicInteger

open class Crates : JavaPlugin() {
  companion object {
    lateinit var instance: Crates //this class instance
    lateinit var config: FileConfiguration //
    lateinit var crateConfigs: HashMap<String, Pair<File, YamlConfiguration>>
    lateinit var questionHead: ItemStack
    val inventories = mutableMapOf<String, Inventory>()
    val crates = mutableMapOf<String, ItemStack>()
    val items = HashMap<String, MutableMap<Int, Pair<ItemStack, Operation>>>()
    val opening = mutableListOf<CrateOpener>()
  }
  
  override fun onEnable() {
    setup()
    server.pluginManager.registerEvents(ClickListener(), this)
    server.pluginManager.registerEvents(InteractListener(), this)
    server.pluginManager.registerEvents(InventoryCloseListener(), this)
    getCommand("crate").executor = CrateCommand()
  }
  
  private fun setup() {
    Crates.instance = this
    Crates.config = config
    saveDefaultConfig()
    crateConfigs = CratesConfig.initiate()
    createInventoriesAndItems()
    questionHead = ItemStack(Material.SKULL_ITEM, 1, SkullType.PLAYER.ordinal.toShort())
    questionHead.itemMeta = (questionHead.itemMeta as SkullMeta).apply {
      owner = "MHF_Question"
      displayName = "&cMystery Item".c()
    }
    
  }
  
  private fun createInventoriesAndItems() {
    crateConfigs.forEach { entry ->
      val crateConfig = entry.value.second
      val title = config.string("${entry.key}.name")
      val inv = Bukkit.createInventory(null, 54, title)
      val itms = mutableMapOf<Int, Pair<ItemStack, Operation>>()
      crateConfig.getConfigurationSection("items").getKeys(false).forEach {
        val prefix = "items.$it"
        val item = itemBuilder {
          dispName = crateConfig.string("$prefix.displayName")
          item = Material.valueOf(crateConfig.string("$prefix.displayItem"))
          amount = crateConfig.getInt("$prefix.amount")
          lore = crateConfig.getStringList("$prefix.lore").c()
        }
        crateConfig.getConfigurationSection("$prefix.enchants").getKeys(false).forEach {
          item.itemMeta = item.itemMeta.apply {
            addEnchant(Enchantment.getByName(it), crateConfig.getInt("$prefix.enchants.$it"), true)
          }
        }
        inv.setItem(crateConfig.getInt("$prefix.previewSlot"), item)
  
        when (crateConfig.string("$prefix.function.type")) {
          "command" -> {
            itms[crateConfig.getInt("$prefix.chance")] =
              Pair(item, Operation(Operation.FunctionType.COMMAND, crateConfig.string("$prefix.function.command")))
          }
          "item" -> {
            val stack = itemBuilder {
              this.item = Material.valueOf(crateConfig.string("$prefix.function.material"))
              this.amount = crateConfig.getInt("$prefix.function.amount")
              this.dispName = crateConfig.string("$prefix.function.name")
              this.lore = crateConfig.getStringList("$prefix.function.lore").c()
            }
  
            stack.itemMeta = stack.itemMeta.apply {
              crateConfig.getConfigurationSection("$prefix.function.enchantment").getKeys(false).forEach {
                addEnchant(Enchantment.getByName(it), crateConfig.getInt("$prefix.function.enchantment.$it"), true)
              }
            }
  
            itms[crateConfig.getInt("$prefix.chance")] = (Pair(item, Operation(Operation.FunctionType.ITEM, item = stack)))
          }
          else -> throw ExceptionInInitializerError("Invalid function type found")
        }
        
      }
      inventories[entry.key] = inv
      crates[entry.key] = itemBuilder {
        dispName = title
        item = Material.valueOf(config.string("${entry.key}.item"))
        lore = config.getStringList("${entry.key}.lore").c()
      }
      items[entry.key] = itms
    }
  }
  
}

data class Operation(val type: FunctionType, val command: String = "", val item: ItemStack = ItemStack(Material.AIR)) {
  enum class FunctionType {
    COMMAND, ITEM
  }
}

data class CrateOpener(val type: String, val user: Player, val inventory: Inventory, val baseBlock: ItemStack, val crate: ItemStack) {
  val items = mutableListOf<Pair<ItemStack, Operation>>()
  init {
    for (i in 1..3) items.add(ChancePicker.getRandomByChance(Crates.items[type]!!))
  }
  
  val step = AtomicInteger()
}