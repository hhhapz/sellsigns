package com.chaoticmc.crates

import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import org.bukkit.inventory.Inventory
import org.bukkit.inventory.ItemStack

class CrateCommand : CommandExecutor {
  override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<String>): Boolean {
    
    if (args.size < 2) send(
      sender,
      "&c/$label give [player|all] [type] [amount]\n/$label preview [type]\n/$label drop [x] [y] [z] [type] [ammount]"
    ) ?: return true
  
    when (args.component1().toLowerCase()) {
      "preview" -> {
        val inv = Crates.inventories[args.component2()]
          ?: send(sender, "&cPlease provide a valid inventory name!")
          ?: return true
        if (sender !is Player) send(sender, "&cYou must be a player to preview!") ?: return true
        val p = sender as Player
        p.openInventory(inv)
      }
      "give" -> {
        if (args.size < 4) send(sender, "&c/$label give [player|all] [type] [amount]") ?: return true
        if (!sender.hasPermission("crates.admin"))
          send(sender, "&cYou don't have permission to do this!")
            ?: return true
        val item: ItemStack = Crates.crates[args.component3()]
          ?: sendC(sender, "&cPlease provide a valid crate name!")
          ?: return true
        item.amount = args.component4().toIntOrNull() ?: 1
        if (args.component2() == "all") for (p in Bukkit.getOnlinePlayers()) giveCrate(p, item, args)
        else {
          val p = Bukkit.getPlayer(args.component2())
          if (p == null || !p.isOnline) send(sender, "&cPlayer not found!") ?: return true
          giveCrate(p, item, args)
        }
      }
      "drop" -> {
        if (args.size < 6) send(sender, "&c/$label drop [x], [y], [z] [type] [amount]") ?: return true
        if (!sender.hasPermission("crates.admin"))
          send(sender, "&cYou don't have permission to do this!")
            ?: return true
        val x = args.component2().toIntOrNull() ?: sendI(sender, "&cPlease provide a valid number!") ?: return true
        val y = args.component3().toIntOrNull() ?: sendI(sender, "&cPlease provide a valid number!") ?: return true
        val z = args.component4().toIntOrNull() ?: sendI(sender, "&cPlease provide a valid number!") ?: return true
        val item: ItemStack = Crates.crates[args.component5()]
          ?: sendC(sender, "&cPlease provide a valid crate name!")
          ?: return true
        item.amount = args[5].toIntOrNull() ?: 1
        (sender as? Player)?.world?.dropItemNaturally(Location(sender.world, x.toDouble(), y.toDouble(), z.toDouble()), item)
          ?: Bukkit.getWorlds().first().dropItemNaturally(Location(Bukkit.getWorlds().first(), x.toDouble(), y.toDouble(), z.toDouble()), item)
        sender.sendMessage("&cThe item has been dropped.".c())
      }
      else -> send(sender, "Please provide a valid argument - /crates give|preview")
    }
    return true
  }
  
  private fun giveCrate(p: Player, item: ItemStack, args: Array<String>) {
    p.sendMessage("&aYou have received ${args.component4().toIntOrNull() ?: 1} ${args.component3()} crate(s)!".c())
    if (p.inventory.firstEmpty() == -1) {
      p.world.dropItem(p.location, item)
      p.sendMessage("&cInventory is full. Dropping to floor.".c())
    } else p.inventory.addItem(item)
  }
  
  private fun send(sender: CommandSender, message: String): Inventory? {
    sender.sendMessage(message.c())
    return null
  }
  
  private fun sendC(sender: CommandSender, message: String): ItemStack? {
    sender.sendMessage(message.c())
    return null
  }
  
  private fun sendI(sender: CommandSender, message: String): Int? {
    sender.sendMessage(message.c())
    return null
  }
}
