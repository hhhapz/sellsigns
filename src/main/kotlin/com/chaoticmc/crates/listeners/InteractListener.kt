package com.chaoticmc.crates.listeners

import com.chaoticmc.crates.*
import org.bukkit.Bukkit
import org.bukkit.Material
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.player.PlayerInteractEvent

class InteractListener : Listener {
  
  @EventHandler
  fun onInteract(e: PlayerInteractEvent) {
    if (!Crates.crates.any { it.value.isSimilar(e.item) }) return
    if (e.action == Action.LEFT_CLICK_AIR || e.action == Action.LEFT_CLICK_BLOCK) {
      Crates.crates.forEach { key, value ->
        e.isCancelled = true
        if (value.isSimilar(e.item)) {
          e.player.openInventory(Crates.inventories[key]!!)
          return@forEach
        }
      }
    }
    
    if (e.action == Action.RIGHT_CLICK_AIR || e.action == Action.RIGHT_CLICK_BLOCK) {
      Crates.crates.forEach { key, value ->
        e.isCancelled = true
        if (value.isSimilar(e.item)) {
          val inv = Bukkit.createInventory(null, 27, Crates.config.string("chestTitle"))
          val item = itemBuilder {
            item = e.item.type
            dispName = Crates.config.string("$key.openanimation.name")
            lore = Crates.config.getStringList("$key.openanimation.lore").c()
          }
          val event = CrateOpener(key, e.player, inv, item, e.player.itemInHand)
          for (i in 0..8) {
            inv.setItem(i, item(Material.STAINED_GLASS_PANE, durability = 7, name = "&f"))
            inv.setItem(i + 9, item)
            inv.setItem(i + 18, item(Material.STAINED_GLASS_PANE, durability = 7, name = "&f"))
          }
          e.player.openInventory(inv)
          Crates.opening.add(event)
          return@forEach
        }
      }
    }
  }
}