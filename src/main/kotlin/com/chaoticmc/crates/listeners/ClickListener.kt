package com.chaoticmc.crates.listeners

import com.chaoticmc.crates.*
import org.bukkit.Bukkit
import org.bukkit.Material
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.inventory.InventoryClickEvent
import org.bukkit.scheduler.BukkitRunnable

class ClickListener : Listener {
  @EventHandler
  fun onClickInPreview(e: InventoryClickEvent) {
    val p = e.whoClicked as Player
    if (Crates.inventories.values.any { it == e.inventory }) e.isCancelled = true
  
    if (Crates.opening.any { it.inventory == e.inventory }) {
      e.isCancelled = true
      val event = Crates.opening.find { it.inventory == e.inventory }!!
      val inv = event.inventory
      if (e.currentItem == null || e.currentItem == Material.AIR || e.currentItem != event.baseBlock) return
      if (event.step.getAndIncrement() == 2) {
      
        val slot = e.slot
        inv.setItem(slot, Crates.questionHead)
        val air = item(Material.STAINED_GLASS_PANE, 1, 8, "&2")
        object : BukkitRunnable() {
          var borderSlot = 0
          var slotLeft = 9
          var slotRight = 17
          override fun run() {
            when {
              borderSlot < 9 -> {
                inv.setItem(borderSlot, air)
                inv.setItem(26 - borderSlot, air)
                borderSlot += 1
              }
              slotLeft <= 13 -> {
                if (inv.getItem(slotLeft) == event.baseBlock) inv.setItem(slotLeft, air)
                if (inv.getItem(slotRight) == event.baseBlock) inv.setItem(slotRight, air)
                slotLeft += 1
                slotRight -= 1
              }
              else -> {
                completeTransaction(event, p)
                cancel()
              }
            }
          }
        }.runTaskTimer(Crates.instance, 5, 4)
      
      } else {
        val slot = e.slot
        inv.setItem(slot, Crates.questionHead)
      }
    }
  }
  
  private fun completeTransaction(event: CrateOpener, p: Player) {
    
    val crate = itemBuilder {
      dispName = event.crate.itemMeta.displayName
      lore = event.crate.itemMeta.lore
      item = event.crate.type
      amount = 1
    }
    p.inventory.removeItem(crate)
    event.items.forEach {
      when (it.second.type) {
        Operation.FunctionType.COMMAND ->
          Bukkit.dispatchCommand(Bukkit.getConsoleSender(), it.second.command.replace("%user%", p.name))
        Operation.FunctionType.ITEM -> {
          if (p.inventory.firstEmpty() == -1) p.world.dropItem(p.location, it.second.item)
          else p.inventory.addItem(it.second.item)
        }
      }
    }
    p.sendMessage(Crates.config.string("receiveEvent").c())
    p.closeInventory()
    p.updateInventory()
    Crates.opening.remove(event)
  }
  
}
